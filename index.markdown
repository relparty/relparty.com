---
# Feel free to add content and custom Front Matter to this file.
# To modify the layout, see https://jekyllrb.com/docs/themes/#overriding-theme-defaults

layout: home
---

{% for publication in site.publications %}
  {% if publication.genesis != true %}
    <h2>{{ publication.name }}</h2>
  {% endif %}

  <p>{{ publication.content | markdownify }}</p>
{% endfor %}