---
name: RelParty
date: 2019-11-16
goal: 1000
layout: default
genesis: true
---

A new sustainable free software funding model.

We enable people to create more and better free software.

Give to receive. Give money. Reach the goal. Release the software to the world.

## How does it work?

There is a new software project or version of an existing project and there is a funding goal.
When the goal is reached, then the software is released. Because it’s free software,
the source code with a copyleft license is also available.


Only free software is accepted for funding.


We manually review the project, source code and publication.


If the goal is not reached, the developer keeps the money.


If the goal is not reached, the source code is automatically released after a year of publication.


We want to enable people to create more and better software libre.